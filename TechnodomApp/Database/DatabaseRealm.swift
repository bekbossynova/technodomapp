//
//  DatabaseRealm.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
let  realm = try! Realm()

public class DatabaseRealm {
    var city: Results<CityModel>?
    
    public static func saveCities(city: [CityModel]) {
        try! realm.write {
            realm.add(city)
        }
    }
    
    public static func updateCityById(cityId: Int, address: AddressModel) {
        let city = realm.objects(CityModel.self).filter("id == \(cityId)").first
        try! realm.write {
            city?.addresses.append(address)
        }
    }
    
  
    public static func getCities() ->[CityModel] {
        let cityList = realm.objects(CityModel.self).toArray(ofType: CityModel.self) as [CityModel]
        return cityList.count > 0 ? cityList : []
    }
     
    
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }

        return array
    }
}
