//
//  BackgroundAlertViewController.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class BackgroundAlertViewController: UIViewController {
    
    let tapGR = UITapGestureRecognizer()
    var dataLamda: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        tapGR.addTarget(self, action: #selector(hideController(_:)))
        view.addGestureRecognizer(tapGR)
    }
    
    @objc func hideController(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: sender.view)
        guard let touchedView = view.hitTest(point, with: nil) else { return }
        if touchedView == view {
            hideAlertScreen(self)
            dataLamda?()
        }
    }
    
    
    
    
}


extension UIViewController: UIGestureRecognizerDelegate {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isKind(of: UIButton.self) {
            return false
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlertScreen(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        
        if let topVC = UIApplication.topViewController() {
            topVC.addChild(viewController)
            topVC.view.addSubview(viewController.view)
            viewController.view.frame = topVC.view.bounds
            
            viewController.view.alpha = 0
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                viewController.view.alpha = 1
            })
        }
    }
    
    func hideAlertScreen(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        
        viewController.willMove(toParent: nil)
        viewController.removeFromParent()
        
        viewController.view.alpha = 1
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            viewController.view.alpha = 0
        }, completion: { (complete: Bool) -> Void in
            viewController.view.removeFromSuperview()
        })
    }
}

extension UIView: UIGestureRecognizerDelegate {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        addGestureRecognizer(tap)
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isKind(of: UIButton.self) {
            return false
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        endEditing(true)
    }
    
    func showAlertScreen(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        
        if let topVC = UIApplication.topViewController() {
            topVC.addChild(viewController)
            topVC.view.addSubview(viewController.view)
            viewController.view.frame = topVC.view.bounds
            
            viewController.view.alpha = 0
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                viewController.view.alpha = 1
            })
        }
    }
    
    
    func hideAlertScreen(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        
        viewController.willMove(toParent: nil)
        viewController.removeFromParent()
        
        viewController.view.alpha = 1
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            viewController.view.alpha = 0
        }, completion: { (complete: Bool) -> Void in
            viewController.view.removeFromSuperview()
        })
    }
    
}

extension UIApplication {
    
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}
