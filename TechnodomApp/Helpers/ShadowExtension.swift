//
//  ShadowExtension.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

extension UIColor {
    static let mainBackground = UIColor(red: 0.949, green: 0.949, blue: 0.949, alpha: 1)
}


extension CALayer {
    func addShadow() {
        shadowOffset = CGSize(width: 0, height: 3)
        shadowOpacity = 0.5
        shadowRadius = 10
        shadowColor = UIColor.red.cgColor
        masksToBounds = false
        if cornerRadius != 0 {
            addShadowWithRoundedCorners()
        }
    }
    
    func roundCorners(radius: CGFloat) {
        cornerRadius = radius
        if shadowOpacity != 0 {
            addShadowWithRoundedCorners()
        }
    }
    
    
    private func addShadowWithRoundedCorners() {
        if let contents = self.contents {
            masksToBounds = false
            sublayers?.filter { $0.frame.equalTo(bounds) }.forEach { $0.roundCorners(radius: cornerRadius) }
            self.contents = nil
            if let sublayer = sublayers?.first, sublayer.name == "contentLayerName" {
                sublayer.removeFromSuperlayer()
            }
            let contentLayer = CALayer()
            contentLayer.name = "contentLayerName"
            contentLayer.contents = contents
            contentLayer.frame = bounds
            contentLayer.cornerRadius = cornerRadius
            contentLayer.masksToBounds = true
            insertSublayer(contentLayer, at: 0)
        }
    }
}



