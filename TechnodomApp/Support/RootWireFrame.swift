//
//  RootWireFrame.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/2/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class RootWireFrame: NSObject {
    
    let homeScreenWireframe: HomeWireframe?
    
    override init() {
        self.homeScreenWireframe = HomeWireframe.sharedInstance
    }
    

    func application(didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?, window: UIWindow) -> Bool {
        self.homeScreenWireframe?.window = window
        self.homeScreenWireframe?.presentHomeViewControllerWindow()
        return true
    }
    
    func scene( willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions, window: UIWindow) {
        self.homeScreenWireframe?.window = window
        self.homeScreenWireframe?.presentHomeViewControllerWindow()
    }

}
