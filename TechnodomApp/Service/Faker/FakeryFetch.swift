//
//  FakeryFetch.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/1/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit
import Fakery

public class FakeryFetch {
    
    static let faker       = Faker(locale: "ru")
    static var cities      = [CityModel]()
    
    public static func getFakeryCities() {
        var i = 1
        while i < 6 {
            let city = CityModel()
            city.name       = faker.address.city()
            city.id         += i
            var s = 1
            while s < 4 {
                let addreses = AddressModel()
                addreses.cityID             = city.id
                addreses.street             = faker.address.streetName()
                addreses.apartmentNumber    = faker.address.buildingNumber()
                s += 1
                city.addresses.append(addreses)
            }
            cities.append(city)
            i += 1
            
        }
        DatabaseRealm.saveCities(city: cities)
        
    }
}
