//
//  HeaderCell.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/1/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

extension HorizontalHeaderVC {
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HeaderView.headerCellId, for: indexPath) as! HeaderView
        let city = savedCities[indexPath.item]
        cell.title.text = city.name
     
        
        let view = UIView(frame: cell.frame)
        view.layer.addShadow()
        view.backgroundColor = UIColor.lightGray
        cell.selectedBackgroundView = view
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width / 3 , height: 50)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return savedCities.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        switch indexPath.item {
        case 0:
            HorizontalHeaderVC.self.navigation?.presentAddCityViewController(horizontalHeaderVC: self)
            didSelectHandler?(self.savedCities[indexPath.item])
        default:
            didSelectHandler?(self.savedCities[indexPath.item])
            print("TAPPED CITY--> \(self.savedCities[indexPath.item])")
            break
            
        }
        
    }
    
}
