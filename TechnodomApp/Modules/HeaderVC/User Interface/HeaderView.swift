//
//  HeaderView.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class HeaderView: UICollectionViewCell {
    
    static let headerCellId = "headerCellId"
    
    let title:  UILabel = {
        let title = UILabel()
        title.textColor = .black
       return title
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor  = .white
        layer.cornerRadius = 12
        clipsToBounds = true
        
        addSubview(title)
        title.centerInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
