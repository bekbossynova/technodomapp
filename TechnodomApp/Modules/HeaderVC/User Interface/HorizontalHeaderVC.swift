//
//  ViewController.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit
import Fakery
import RealmSwift


class HorizontalHeaderVC: HorizontalSnnapingController, UICollectionViewDelegateFlowLayout {
    
    var addresses   = [AddressModel]()
    var savedCities = DatabaseRealm.getCities()
    var didSelectHandler: ((CityModel) -> ())?
    
    static var navigation: AddCityWireframe?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .mainBackground
        collectionView.contentInset = .init(top: 16, left: 16, bottom: 16, right: 0)
        collectionView.register(HeaderView.self, forCellWithReuseIdentifier: HeaderView.headerCellId)
        
        if savedCities.count < 5 {
            FakeryFetch.getFakeryCities()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.savedCities = DatabaseRealm.getCities()
                self.addButtonFetch()
            }
            
        } else {
            addButtonFetch()
        }
        
    }
    
    
    func addButtonFetch() {
        let firstIndex = CityModel()
        firstIndex.name = "Добавить"
        self.savedCities.insert(firstIndex, at: 0)
    }
    
    
    
}


