//
//  HorizontalHeaderView.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class HorizontalHeaderView: UICollectionReusableView {
    
    static let horizontalCell = "horizontalCell"
    var headerView = HorizontalHeaderVC()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 12
        clipsToBounds = true
        addSubview(headerView.view)
        headerView.view.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
