//
//  AddView.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class AddView: UIView {

    
    let addressTF:  UITextField = {
        let tf = UITextField()
        tf.placeholder = "Адресс"
        return tf
    }()
    let cityTF:  UITextField = {
        let tf = UITextField()
        tf.placeholder = "Город"
        return tf
    }()
    
    let apartmentTF:  UITextField = {
        let tf = UITextField()
        tf.placeholder = "кв"
        tf.constrainWidth(constant: 50)
        return tf
    }()
   
    let addButton:  UIButton = {
        let button = UIButton()
        button.setTitle("Добавить", for: .normal)
        button.backgroundColor = .lightGray
        button.constrainHeight(constant: 40)
        button.layer.cornerRadius = 12
        button.clipsToBounds = true
        return button
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 12.5
        clipsToBounds = true
        showTFConf()
        setLayouts()
        
        
    }
    
    func setLayouts() {
            
        
        addSubview(cityTF)
        cityTF.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 16, left: 16, bottom: 0, right: 16))
        
        addSubview(addressTF)
        addressTF.anchor(top: cityTF.bottomAnchor, leading: cityTF.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        
        addSubview(apartmentTF)
        apartmentTF.anchor(top: cityTF.bottomAnchor, leading: addressTF.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 10, left: 16, bottom: 0, right: 16))
        
        addSubview(addButton)
        addButton.anchor(top: addressTF.bottomAnchor, leading: cityTF.leadingAnchor, bottom: bottomAnchor, trailing: cityTF.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 0))
    }
    
    func showTFConf() {
        let tfArray = [addressTF, cityTF, apartmentTF]
        tfArray.forEach { (tf) in
            tf.layer.borderWidth = 1
            tf.setLeftPaddingPoints(10)
            tf.setRightPaddingPoints(10)
            tf.constrainHeight(constant: 40)
            tf.textColor = .black
            tf.layer.borderColor = UIColor.lightGray.cgColor
            
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
