//
//  AddCityVC.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class AddCityVC: UIViewController {
    
    let addView = AddView()
    var checkedCity: CityModel?
    
    var cities = DatabaseRealm.getCities()
    var textFieldEmpty = false
    var id = 1

    
    static var navigation: AddCityWireframe?
    var interactor: AddCityInteractor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayouts()
        textFieldAction()
        view.isOpaque = false
        view.backgroundColor = UIColor.init(white: 0.4, alpha: 0.8)
        
        let tapID = UITapGestureRecognizer(target: self, action: #selector(handleTaptoDismis(_:)))
        view.addGestureRecognizer(tapID)
        
    }
    
    func setLayouts() {
        view.addSubview(addView)
        addView.centerInSuperview(size: .init(width: 300, height: 166))
        let pickerView = UIPickerView()
        pickerView.delegate = self
        addView.cityTF.inputView = pickerView
        
        
    }
    
    fileprivate func textFieldAction() {
        addView.cityTF.addTarget(self, action: #selector(handleTextAdded), for: .allEditingEvents)
        addView.addressTF.addTarget(self, action: #selector(handleTextAdded), for: .allEditingEvents)
        addView.apartmentTF.addTarget(self, action: #selector(handleTextAdded), for: .allEditingEvents)
        addView.addButton.addTarget(self, action: #selector(saveTextFieldData), for: .touchUpInside)
        
    }
    
    @objc func handleTextAdded() {
        let validate = addView.cityTF.text!.count > 1 && addView.addressTF.text!.count > 1 && addView.apartmentTF.text!.count >= 1
        
        if validate {
            textFieldEmpty = false
        } else {
            textFieldEmpty = true
        }
        
    }
    
    @objc func saveTextFieldData() {
        if textFieldEmpty != true {
            let address = AddressModel()
            address.cityID = id
            address.street = addView.addressTF.text ?? ""
            address.apartmentNumber = addView.apartmentTF.text ?? ""
            DatabaseRealm.updateCityById(cityId: self.id, address: address)
            let alert = UIAlertController(title: "Адресс", message: "успешно сохранен", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                self.addView.addressTF.text     = ""
                self.addView.cityTF.text        = ""
                self.addView.apartmentTF.text   = ""
                self.textFieldEmpty = true
                
            }))
            self.present(alert, animated: true)
            
        } else {
            let alert = UIAlertController(title: "Внимание!", message: "Необходимо заполнить все поля", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
           
        }
    }

    
    @objc  func handleTaptoDismis(_ sender: UITapGestureRecognizer) {
        AddCityVC.navigation?.dismissAddCityViewController(addCityVC: self)
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let touchView = touch.view, touchView.isDescendant(of: self.view) {
            return false
        }
        
        return true
    }
    
    
}

