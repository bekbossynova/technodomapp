//
//  AddCityProtocol.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/2/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

@objc protocol AddCityProtocol {
    func addCity() 
}

