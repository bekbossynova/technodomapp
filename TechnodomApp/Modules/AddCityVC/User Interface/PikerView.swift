//
//  PikerView.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/1/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import  UIKit

extension AddCityVC: UIPickerViewDataSource, UIPickerViewDelegate  {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cities.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cities[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        addView.cityTF.text = cities[row].name
        self.id = cities[row].id
        
    }
    
    
}


