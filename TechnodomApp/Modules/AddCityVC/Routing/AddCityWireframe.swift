//
//  AddCityWireframe.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/2/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class AddCityWireframe: NSObject, AddCityWireframeProtocol {
    
    //var addCityVC: AddCityVC?
//    var headerVC: HorizaontalHeaderVC?
    
    func dismissAddCityViewController(addCityVC: AddCityVC) {
        addCityVC.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func presentAddCityViewController(horizontalHeaderVC: HorizontalHeaderVC) {
        let addCityVC = AddCityVC()
        addCityVC.modalPresentationStyle = .custom
        horizontalHeaderVC.present(addCityVC, animated: true, completion: nil)
    }
}
