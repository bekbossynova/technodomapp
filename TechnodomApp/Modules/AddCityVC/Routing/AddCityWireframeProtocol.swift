//
//  AddCityWireframeProtocol.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/2/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation

@objc protocol AddCityWireframeProtocol {
    
    func dismissAddCityViewController(addCityVC: AddCityVC)
    func presentAddCityViewController(horizontalHeaderVC: HorizontalHeaderVC)
}

