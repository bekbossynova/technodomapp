//
//  HomeWireframe.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/2/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class HomeWireframe: NSObject, HomeWireframeProtocol {
    static let sharedInstance = HomeWireframe()
    var homeViewController: HomeViewController?
    var headerViewController: HorizontalHeaderVC?
    var window: UIWindow?

    
    func presentHomeViewControllerWindow() {
        let homeVC = HomeViewController()
        let headerVC = HorizontalHeaderVC()
        let headerCell = HorizontalHeaderView()
        
        self.homeViewController = homeVC
        self.homeViewController?.headerCell = headerCell
        self.homeViewController?.headerCell?.headerView = headerVC
        
        let cityWireframe = AddCityWireframe()
        HorizontalHeaderVC.navigation = cityWireframe
        AddCityVC.navigation = cityWireframe
        self.window!.rootViewController = homeViewController
        self.window!.makeKeyAndVisible()
    }
}
