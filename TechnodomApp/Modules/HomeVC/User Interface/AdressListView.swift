//
//  AdressListView.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class AdressListView: UICollectionViewCell {
    
    static let adressCellId = "adressCellId"
    
    var adressLabel = UILabel()
    var apartmenLabel = UILabel()
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        let array = [adressLabel, apartmenLabel]
        array.forEach { (label) in
            label.text = "text"
            label.textColor = .black
            label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        }
        
        let stackView = UIStackView(arrangedSubviews: [
            adressLabel,
            apartmenLabel
        ])
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.distribution = .fill
        
        
       addSubview(stackView)
        stackView.centerInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
