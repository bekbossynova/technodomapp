//
//  AddressCell.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/1/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

extension HomeViewController: UICollectionViewDelegateFlowLayout{
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let addressCell = collectionView.dequeueReusableCell(withReuseIdentifier: AdressListView.adressCellId, for: indexPath) as! AdressListView
        
        addressCell.layer.cornerRadius = 12
        addressCell.clipsToBounds = true
        
        let addresses = self.currentCityAddresses[indexPath.item]
        
        guard let street = addresses.value(forKey: "street") else { return addressCell }
        addressCell.adressLabel.text =  String(describing: street)
        
        guard let apartmentNumber = addresses.value(forKey: "apartmentNumber") else {return addressCell}
        addressCell.apartmenLabel.text = String(describing: apartmentNumber)
        
        
        return  addressCell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return currentCityAddresses.count
    }

    
}
