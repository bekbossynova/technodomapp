//
//  AddressHeader.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/1/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

extension HomeViewController {
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        self.headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: HorizontalHeaderView.horizontalCell, for: indexPath) as! HorizontalHeaderView
        
        headerCell?.headerView.didSelectHandler.self = { [weak self]  city in
            self?.currentCityAddresses = city.addresses
            self?.collectionView.reloadData()
        }
        return headerCell!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width  , height: 100)
    }
}
