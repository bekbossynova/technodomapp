//
//  ViewController.swift
//  TechnodomApp
//
//  Created by bekbossynova on 8/1/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit
import RealmSwift

class HomeViewController: UICollectionViewController {
    
    var currentCityAddresses = List<AddressModel>()
    var headerCell: HorizontalHeaderView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .mainBackground
        registerAddressCollectionCell()
       
    }
    
    func registerAddressCollectionCell() {
        collectionView.register(HorizontalHeaderView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: HorizontalHeaderView.horizontalCell)
        
        collectionView.register(AdressListView.self,
                                forCellWithReuseIdentifier: AdressListView.adressCellId)
    }
    
    
    
    
    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}



