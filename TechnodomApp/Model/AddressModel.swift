//
//  AddressModel.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/31/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

public class AddressModel: Object {
    @objc var cityID: Int = 0
    @objc var street: String = ""
    @objc var apartmentNumber: String = ""
}
