//
//  RealmModel.swift
//  TechnodomApp
//
//  Created by bekbossynova on 7/28/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

public class CityModel: Object {
    @objc dynamic var name: String      = ""
    @objc dynamic var id: Int           = 0
    var addresses = List<AddressModel>()
    
    public override static func primaryKey() -> String? {
        return "id"
    }
   
}

